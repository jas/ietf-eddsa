D=draft-josefsson-eddsa-ed25519

# To prevent xml2rfc from opening any windows
unexport DISPLAY

all: $(D).txt $(D).html

clean:
	rm *~ $(D).txt $(D).html

$(D).txt: $(D).xml
	xml2rfc $<

$(D).html: $(D).xml
	xml2rfc $< $@

check:
	cd src && $(MAKE) $@
