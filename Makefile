D=draft-irtf-cfrg-eddsa
PYTHON=python3
INLINES=src/eddsa2-test-ed25519.xml src/eddsa2.xml include/ed25519-test-vectors.xml include/ed25519ph-test-vectors.xml\
	include/ed448-test-vectors.xml include/ed448ph-test-vectors.xml include/ed25519ctx-test-vectors.xml
DONT_DELETE=$(patsubst include/%.xml,include/%.raw,$(INLINES)) $(patsubst include/%.xml,include/%.txt,$(INLINES))

# To prevent xml2rfc from opening any windows
unexport DISPLAY

CROSSCHECK_EXECUTABLE=true
export CROSSCHECK_EXECUTABLE

all: $(D).txt $(D).html $(D)-from--00.diff.html

export: out/$(D).xml

clean:
	rm -f *~ $(D).txt $(D).html $(DONT_DELETE)

$(D).txt: $(D).xml $(INLINES)
	xml2rfc $<

$(D).html: $(D).xml $(INLINES)
	xml2rfc --html $< $@

$(D)-from--00.diff.html: $(D).txt
	rfcdiff $(D)-00.txt $(D).txt 

out/$(D).xml: $(D).xml $(INLINES)
	xmllint --noent $< | egrep -v "<!ENTITY .* SYSTEM .*>" >$@
	xmllint $@ >/dev/null

check: $(INLINES)
	cd src && $(MAKE) $@

include/%.xml: include/%.txt src/txt2xml.py
	$(PYTHON) src/txt2xml.py $< $@

include/%.txt: include/%.raw src/fmt-testvectors.py
	$(PYTHON) src/fmt-testvectors.py $< $@

include/%.raw: include/%.in src/make-testvectors.py
	$(PYTHON) src/make-testvectors.py $< $@

src/%.xml: src/%.py src/txt2xml.py
	$(PYTHON) src/txt2xml.py $< $@

.precious: $(DONT_DELETE)
