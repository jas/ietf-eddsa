Conventions:
============
- Octet is 8 bits.
- Word is 64 bits.
- a^b: Bitwise XOR between a and b.
- ~a: Bitwise NOT of a.
- a&b: Bitwise AND between a and b.
- x%y: Remainder of x divided by y.
- ROL(x, b): Word x rotated left by b places.
- Indexes are numbered starting from zero.


Variants:
=========
The parameters for variants are as follows:
Type: Type of function
r_b: Number of octets per input block.
r_w: Number of words per input block.
e_b: Number of octets out output
o_p: First padding octet (hexadecimal)
CR: Collision resistance in bits (max. 4 bits/octet of output)
PIR: Preimage resistance in bits (max. 8 bits/octet of output)

      SHA3-224  SHA3-256  SHA3-384  SHA3-512  SHAKE128  SHAKE256
Type: Hash      Hash      Hash      Hash      XOF       XOF
r_b:  144       136       104       72        168       136
r_w:  18        17        13        9         21        17
e_b:  28        32        48        64        variable  variable
o_p:  06        06        06        06        1F        1F
CR:   112       128       192       256       <=128     <=256
PIR:  224       256       384       512       <=128     <=256


Algorithm Transform:
====================
Input: state: a 25 word array, mutated.

Perform 24 rounds (round goes from 0 to 23) of following:

1) Compute 5-element array c, where c[i] is bitwise XOR of all
   state words with index modulo 5 equal to i.
2) Compute 5-element array d, where:
   d[i] <- c[(i+4)%5] ^ ROT(c[(i+1)%5], 1)
3) Update state by state[i] <- state[i] ^ d[i%5] for i from 0 to 24.
4) Update state by state[i] <- ROL(state[i], rotations[i]) for i from
   0 to 24.
5) Update state by state[i] <- state[permutation[i]] for i from 0 to
   24. All assignments in this step occur simultaneously, so it acts
   as a permutation of words.
6) Update state by state[i] <- state[i] ^ ((~state[r(i)]) &
   state[r(r(i))]) for i from 0 to 24. r(i) = i + (i+1)%5 - i%5.
   All assignments in this step occur simultaneously, so all inputs
   to bit operations are pre-update values.
7) Update state by state[0] <- state[0] ^ RC[round].


The array rotations[i] is:
  |x0 x1 x2 x3 x4 x5 x6 x7 x8 x9
--+-----------------------------
0x| 0  1 62 28 27 36 44  6 55 20
1x| 3 10 43 25 39 41 45 15 21  8
2x|18  2 61 56 14

The array permutation[i] is:
  |x0 x1 x2 x3 x4 x5 x6 x7 x8 x9
--+-----------------------------
0x| 0  6 12 18 24  3  9 10 16 22
1x| 1  7 13 19 20  4  5 11 17 23    
2x| 2  8 14 15 21 

This can also expressed as two orbits:
- Word 0 stays put.
- All other 24 words shift one place in cycle:
  1->10->7->11->17->18->3->5->16->8->21->24->
  4->15->23->19->13->12->2->20->14->22->9->6->1

The array RC[i] is:
round#          value (hex):
 0              0x0000000000000001
 1              0x0000000000008082
 2              0x800000000000808a
 3              0x8000000080008000
 4              0x000000000000808b
 5              0x0000000080000001
 6              0x8000000080008081
 7              0x8000000000008009
 8              0x000000000000008a
 9              0x0000000000000088
10              0x0000000080008009
11              0x000000008000000a
12              0x000000008000808b
13              0x800000000000008b
14              0x8000000000008089
15              0x8000000000008003
16              0x8000000000008002
17              0x8000000000000080
18              0x000000000000800a
19              0x800000008000000a
20              0x8000000080008081
21              0x8000000000008080
22              0x0000000080000001
23              0x8000000080008008

Algorithm RawSha3:
==================
Input: message: an octet string
Input: r_w: An integer 1 <= r_w <= 24.
Input: o_p: An octet
Input: e_b: The desired length of output
Output: an octet string of e_b octets.

1) Let r_b <- 8 * r_w
2) Let index <- 0
3) Create 25 word vector state, where state[i] <- 0
4) While at least r_b octets of message remain:
   a) Let m be the next r_b octets of message.
   b) Reinterpret array of octets m as array of little-endian words m'.
   c) Update state by doing state[i] <- state[i] ^ m'[i], for i from 0
      to r_w-1.
   d) Transform the state using the transform function.
5) Let m be the rest of the message.
6) Append octet o_p to m.
7) Until m is r_b octets, append zero octet to m.
8) Set the most significant bit of the last octet of m.
9) Reinterpret array of octets m as array of little-endian words m'.
10) Update state by doing state[i] <- state[i] ^ m'[i], for i from 0
    to r_w-1.
11) Transform the state using the transform function.
12) Set o to empty.
13) While o is shorter than e_b octets:
    a) Let b' be the first r_w words of state.
    b) Reinterpret array of little-endian words b' as array of octets
       b.
    c) Append b to o.
    d) Transform the state using the transform function.
14) Return o truncated to e_b octets.


The values of inputs r_w, o_p and e_b for various hashes and XOFs
are given below:

Algorithm:   r_w    o_p   e_b
SHA3-224     18      6    28
SHA3-256     17      6    32
SHA3-384     13      6    48
SHA3-512      9      6    64
SHAKE128     21     31    *
SHAKE256     17     31    *

* => The value is variable, needs to be input 
