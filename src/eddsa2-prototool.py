#!/usr/bin/python3
from eddsa2 import eddsa_obj
import sys;

def read_data_file(filename):
    with open(filename,"rb") as filp:
        return filp.read()

def read_hex(ch):
    ch=ord(ch)
    if ch>=65 and ch<=70:return ch-55
    if ch>=97 and ch<=102:return ch-87
    if ch>=48 and ch<=57:return ch-48
    if ch==9 or ch==10 or ch==11 or ch==13 or ch==32:return False
    return None

def read_data_hex(string):
    rfn=read_hex
    shift=0
    modulus=0
    out=b""
    for i in range(0,len(string)):
        code=rfn(string[i])
        if code is False:
            pass	#Skip character
        elif code is None:
            print("Invalid character in Hex");
            sys.exit(2)
        else:
            shift=16*shift+code
            modulus=(modulus+1)&1
            if modulus==0:
                out+=bytes([shift&0xFF])
                shift=0
    if modulus==1:
        print("Invalid hex length 1(mod2)");
        sys.exit(2)
    return out

def read_b64(ch):
    ch=ord(ch)
    if ch>=65 and ch<=90:return ch-65
    if ch>=97 and ch<=122:return ch-71
    if ch>=48 and ch<=57:return ch+4
    if ch==43:return 62
    if ch==47:return 63
    if ch==9 or ch==10 or ch==11 or ch==13 or ch==32:return False
    if ch==61:return True
    return None

def read_b64url(ch):
    ch=ord(ch)
    if ch>=65 and ch<=90:return ch-65
    if ch>=97 and ch<=122:return ch-71
    if ch>=48 and ch<=57:return ch+4
    if ch==45:return 62
    if ch==95:return 63
    if ch==9 or ch==10 or ch==11 or ch==13 or ch==32:return False
    return None

def read_data_base64(string,urlsafe):
    rfn=read_b64url if urlsafe else read_b64
    shift=0
    modulus=0
    out=b""
    end=False
    for i in range(0,len(string)):
        code=rfn(string[i])
        if code is False:
            pass	#Skip character
        elif code is True:
            end=True
        elif code is None:
            print("Invalid character in Base64");
            sys.exit(2)
        else:
            if end:
                print("Invalid character in Base64");
                sys.exit(2)
            shift=64*shift+code
            modulus=(modulus+1)&3
            if modulus==0:
                out+=bytes([(shift>>16)&0xFF,(shift>>8)&0xFF,\
                    shift&0xFF])
                shift=0
    if modulus==1:
        print("Invalid Base64 length 1(mod4)");
        sys.exit(2)
    if modulus==2:
        out+=bytes([(shift>>4)&0xFF])
    if modulus==3:
        out+=bytes([(shift>>10)&0xFF,(shift>>2)&0xFF])
    return out

def read_data_utf8(utf8):
    return utf8.encode("utf-8")


def read_data(locator):
    if len(locator)>=5 and locator[:5] == "file:":
        return read_data_file(locator[5:])
    elif len(locator)>=4 and locator[:4] == "hex:":
        return read_data_hex(locator[4:])
    elif len(locator)>=7 and locator[:7] == "base64:":
        return read_data_base64(locator[7:],False)
    elif len(locator)>=10 and locator[:10] == "base64url:":
        return read_data_base64(locator[10:],True)
    elif len(locator)>=5 and locator[:5] == "utf8:":
        return read_data_utf8(locator[5:])
    else:
        print("Unknown load location '{:s}'!".format(locator))
        sys.exit(1);

def write_data_file(filename,data):
    with open(filename,"wb") as filp:
        filp.write(data)

def write_data_utf8(name,data):
    try:
        data = data.decode('utf-8')
        print("{:s}: {:s}".format(name, data))
    except UnicodeDecodeError:
        print("<Error: Can't represent field '{:s}' as string>".\
	    format(name))

B64CH=\
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ"\
    "abcdefghijklmnopqrstuvwxyz"\
    "0123456789"\
    "+/"
B64URLCH=\
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ"\
    "abcdefghijklmnopqrstuvwxyz"\
    "0123456789"\
    "-_"

HEXCH="0123456789abcdef"
    
def write_data_hex(name,data,spacing):
    out=name+": "
    for i in range(0,len(data)):
       out+=HEXCH[(data[i]>>4)&15]
       out+=HEXCH[(data[i]>>0)&15]
       if spacing>0 and i>0 and i%spacing==0:
           out+=" "
    print(out)

def write_data_base64(name,data,urlsafe):
    pad = "" if urlsafe else "="
    charset = B64URLCH if urlsafe else B64CH
    out=name+": "
    shift=0
    modulus=0
    for i in range(0,len(data)):
        shift=256*shift+data[i]
        modulus=(modulus+1)%3
        if modulus == 0:
            out+=charset[(shift>>18)&63]
            out+=charset[(shift>>12)&63]
            out+=charset[(shift>>6)&63]
            out+=charset[(shift>>0)&63]
            shift=0
    if modulus == 2:
        out+=charset[(shift>>10)&63]
        out+=charset[(shift>>4)&63]
        out+=charset[4*(shift>>0)&63]
    elif modulus == 1:
        out+=charset[(shift>>2)&63]
        out+=charset[16*(shift>>0)&63]
    out+=pad*((3-modulus)%3)
    print(out)

def write_data(locator,name,data):
    if len(locator)>=5 and locator[:5] == "file:":
        write_data_file(locator[5:],data)
    elif locator == "hex":
        write_data_hex(name,data,0)
    elif locator == "hex2":
        write_data_hex(name,data,1)
    elif locator == "hex4":
        write_data_hex(name,data,2)
    elif locator == "hex8":
        write_data_hex(name,data,4)
    elif locator == "base64":
        write_data_base64(name,data,False)
    elif locator == "base64url":
        write_data_base64(name,data,True)
    elif locator == "utf8":
        write_data_utf8(name,data)
    elif locator == "null":
         pass	#Throw it.
    else:
        print("Unknown save location '{:s}'!".format(locator))
        sys.exit(1)

def print_file_out_help():
    print("Output files can be specified as:")
    print("file:<name> -> Save to file <name>")
    print("base64 -> Print to screen as base64")
    print("base64url -> Print to screen as base64url")
    print("hex -> Print to screen as hex")
    print("hex2 -> Print to screen as hex with 2 spacing")
    print("hex4 -> Print to screen as hex with 4 spacing")
    print("hex8 -> Print to screen as hex with 8 spacing")
    print("utf8 -> Print to screen as UTF-8")
    print("null -> Throw away the output")

def print_file_in_help():
    print("Input files can be specified as:")
    print("file:<name> -> Read from file <name>")
    print("utf8:<content> -> Decode <content> as UTF-8 and use it")
    print("hex:<content> -> Decode <content> as hex and use it")
    print("base64:<content> -> Decode <content> as base64 and use it")
    print("base64url:<content> -> Decode <content> as base64url "\
        "and use it")

def do_id(args):
    if args[0] == "--help" or len(args) < 2:
        print("id: Identity function")
        print("id <in> <out>");
        print("<in> -> Input")
        print("<out> -> Output")
        print_file_in_help()
        print_file_out_help()
        return 1
    data = read_data(args[0])
    write_data(args[1],"out",data)
    return 0

def do_keygen(args):
    if args[0] == "--help" or len(args) < 3:
        print("keygen: Generate a keypair")
        print("keygen <scheme> <priv-out> <pub-out>");
        print("<scheme> -> Scheme, Ed25519 or Ed448.")
        print("<priv-out> -> Private key output file")
        print("<pub-out> -> Public key output file")
        print_file_out_help()
        return 1
    privkey,pubkey = eddsa_obj(args[0]).keygen(None);
    write_data(args[1],"privkey",privkey)
    write_data(args[2],"pubkey",pubkey)
    return 0

def do_topub(args):
    if args[0] == "--help" or len(args) < 3:
        print("topub: Take private key and make public key")
        print("topub <scheme> <priv-in> <pub-out>");
        print("<scheme> -> Scheme, Ed25519 or Ed448.")
        print("<priv-in> -> Private key input file")
        print("<pub-out> -> Public key output file")
        print_file_in_help()
        print_file_out_help()
        return 1
    privkey = read_data(args[1])
    write_data("base64","privkey",privkey)
    _privkey,pubkey = eddsa_obj(args[0]).keygen(privkey)
    write_data(args[2],"pubkey",pubkey)
    return 0

def do_sign(args):
    if args[0] == "--help" or len(args) < 4 or \
	    (args[1][:10] == "--context=" and len(args) < 5):
        print("sign: Sign data")
        print("sign <scheme> [--context=<context-in>] <priv-in> "\
            "<message-in> <signature-out>");
        print("<scheme> -> Scheme, Ed25519 or Ed448.")
        print("--context=<context-in> -> Use specified context input "\
            "instead of blank")
        print("<priv-in> -> Private key input file")
        print("<message-in> -> Message input file")
        print("<signature-out> -> Signature output file")
        print_file_in_help()
        print_file_out_help()
        return 1
    scheme = eddsa_obj(args[0])
    base = 1
    context = None
    if args[1][:10] == "--context=":
        context = read_data(args[1][10:])
        base = 2
    privkey = read_data(args[base])
    message = read_data(args[base+1])
    _privkey,pubkey = scheme.keygen(privkey)
    signature = scheme.sign(privkey,pubkey,message,context)
    write_data(args[base+2],"signature",signature)
    return 0

def do_verify(args):
    if args[0] == "--help" or len(args) < 4 or \
	    (args[1][:10] == "--context=" and len(args) < 5):
        print("verify: Verify data")
        print("verify <scheme> [--context=<context-in>] <pub-in> "\
            "<message-in> <signature-in>");
        print("<scheme> -> Scheme, Ed25519 or Ed448.")
        print("--context=<context-in> -> Use specified context input "\
            "instead of blank")
        print("<pub-in> -> Public key input file")
        print("<message-in> -> Message input file")
        print("<signature-in> -> Signature input file")
        print_file_in_help()
        sys.exit(1)
    scheme = eddsa_obj(args[0])
    base = 1
    context = None
    if args[1][:10] == "--context=":
        context = read_data(args[1][10:])
        base = 2
    pubkey = read_data(args[base])
    message = read_data(args[base+1])
    signature = read_data(args[base+2])
    if scheme.verify(pubkey,message,signature,context):
        return 0
    else:
        print("Signature ERROR")
        return 2
   
commands = { \
    "keygen":do_keygen,\
    "topub":do_topub,\
    "sign":do_sign,\
    "verify":do_verify,\
    "id":do_id,\
}

if len(sys.argv) == 1 or sys.argv[1] not in commands:
    print("Unknown command. Known commands:")
    for i in commands:
        print(i)
    sys.exit(1)
sys.exit(commands[sys.argv[1]](sys.argv[2:]))
