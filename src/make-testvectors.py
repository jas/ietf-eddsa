from sys import argv
from eddsa2 import eddsa_obj,self_check_curves


def bytes_to_hex(b):
    s = ""
    for i in b: s += "{:02x}".format(i)
    return s

def spec_to_bytes(s):
    parts = s.split(":")
    #Hexadecimal dump.
    if parts[0] == "H": return bytes.fromhex(parts[1])
    #UTF-8 String.
    if parts[0] == "S": return parts[1].encode("utf-8")
    #Pseudorandom via LCG.
    if parts[0] == "L":
        out = bytearray()
        state = int(parts[1])
        count = int(parts[2])
        for i in range(0, count):
            #Constants From Knuth's MMIX.
            state = (6364136223846793005*state+1442695040888963407)%2**64
            out.append(state>>56)
        return out
    raise NotImplementedError("Spec not implemented")

#Don't use really busted curves...
self_check_curves()

with open(argv[1],"r") as infile:
    with open(argv[2],"w") as outfile:
        for line in infile:
            parts=line.strip().split(";")
            algo = eddsa_obj(parts[1])
            privkey=spec_to_bytes(parts[2])
            _privkey,pubkey=algo.keygen(privkey)
            _pubkey = bytes_to_hex(pubkey)
            message=spec_to_bytes(parts[3])
            context=spec_to_bytes(parts[4])
            signature = algo.sign(privkey, pubkey, message, context)
            assert algo.verify(pubkey, message, signature, context)
            _signature = bytes_to_hex(signature)
            s = "{:s};{:s};{:s};{:s};{:s};{:s};{:s}\n".format(parts[0], \
                parts[1], bytes_to_hex(privkey), _pubkey, \
                bytes_to_hex(message),_signature, bytes_to_hex(context))
            outfile.write(s)
