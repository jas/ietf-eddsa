from sys import argv
from eddsa2 import eddsa_obj

def munge_string(s, pos, change):
    return (s[:pos] +
            int.to_bytes(s[pos] ^ change, 1, "little") +
            s[pos+1:])

for fname in argv[1:]:
    with open(fname,"r") as infile:
        for line in infile:
            parts=line.strip().split(";")
            print(parts[0])
            algo = eddsa_obj(parts[1])
            privkey = bytes.fromhex(parts[2])
            pubkey = bytes.fromhex(parts[3])
            message = bytes.fromhex(parts[4])
            signature = bytes.fromhex(parts[5])
            context = bytes.fromhex(parts[6])
            _privkey,_pubkey = algo.keygen(privkey)
            assert _pubkey == pubkey
            _signature = algo.sign(privkey,pubkey,message,context)
            assert _signature == signature
            assert algo.verify(pubkey,message,signature,context)
            if len(message) == 0:
                bad_msg = b"x"
            else:
                bad_msg = munge_string(message, len(message) // 3, 4)
            assert not algo.verify(pubkey, bad_msg, signature)
            bad_signature = munge_string(signature, len(signature) // 3, 8)
            assert not algo.verify(pubkey, message, bad_signature)
            bad_signature = munge_string(signature, 2 * len(signature) // 3, 16)
            assert not algo.verify(pubkey, message, bad_signature)
