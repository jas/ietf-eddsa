from sys import argv

def fmt_hexblock(block):
    s = ""
    while len(block) >= 32:
        s += block[:32] + "\n"
        block = block[32:]
    if len(block) > 0:
        s += block + "\n"
    return s

   
first = True
with open(argv[1],"r") as infile:
    with open(argv[2],"w") as outfile:
        for line in infile:
            parts=line.strip().split(";")
            s = "" if first else "\n"
            s += "-----{:s}\n".format(parts[0]);
            s += "\nALGORITHM:\n"
            s += fmt_hexblock(parts[1]);
            s += "\nSECRET KEY:\n"
            s += fmt_hexblock(parts[2]);
            s += "\nPUBLIC KEY:\n"
            s += fmt_hexblock(parts[3]);
            plural = "s" if len(parts[4])//2 != 1 else ""
            s += "\nMESSAGE (length {:d} byte{:s}):\n".format(len(parts[4])//2, plural)
            s += fmt_hexblock(parts[4]);
            if parts[6] != "":
                s += "\nCONTEXT:\n"
                s += fmt_hexblock(parts[6]);
            s += "\nSIGNATURE:\n"
            s += fmt_hexblock(parts[5]);
            outfile.write(s)
            first = False
        outfile.write("-----\n")
