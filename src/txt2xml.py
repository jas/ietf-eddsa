from sys import argv

repl=[["&","&amp;"],["\"","&quot;"],["'","&apos;"],["<","&lt;"],[">","&gt;"]]

with open(argv[1],"r") as infile:
    with open(argv[2],"w") as outfile:
        for line in infile:
            for r in repl:
                line = line.replace(r[0], r[1])
            outfile.write(line)
